
package principal.mapas;

import java.awt.Rectangle;
import principal.sprites.Sprite;

public class Tale {
    
    private final Sprite sprite;
    private final int id;
    private boolean solido;

    //Constructores
    public Tale(final Sprite sprite, int id) {
        this.sprite = sprite;
        this.id = id;
        solido = false;
    }
    
    public Tale(final Sprite sprite, int id, boolean solido) {
        this.sprite = sprite;
        this.id = id;
        this.solido = solido;
    }

    //getters 
    
    public Sprite obtenerSprite() {
        return sprite;
    }
    
    public int obtenerId() {
        return id;
    }

    //setter
    public void establecerSolido(final boolean solido) {
        this.solido = solido;
    }
    
    public Rectangle obtenerLimites(final int x, final int y){
        return new Rectangle(x, y, sprite.obtenerAncho(), sprite.obtenerAlto());
    }
    
}


package principal.graficos;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;
import principal.Constantes;
import principal.GestorPrincipal;
import principal.control.GestorControles;
import principal.control.Raton;
import principal.herramientas.DatosDebug;
import principal.herramientas.DibujoDebug;
import principal.maquinaestados.GestorEstados;

public class SuperficieDibujo extends Canvas{
    
    private static final long serialVersionUID = 456L;
    
    private int ancho;
    private int alto;
    
    private Raton raton;
    
    //Constructor
    public SuperficieDibujo(final int ancho, final int alto) {
        this.ancho = ancho;
        this.alto = alto;
        
        this.raton = new Raton(this);
        
        setIgnoreRepaint(true);
        setCursor(raton.obtenerCursor());
        setPreferredSize(new Dimension(ancho, alto));
        addKeyListener(GestorControles.teclado);
        addMouseListener(raton);
        setFocusable(true);
        requestFocus();
    }
    
    //Metodos
    public void dibujar(final GestorEstados ge){
        final BufferStrategy buffer = getBufferStrategy();
        if(buffer == null){
            createBufferStrategy(4);
            return;
        }
        
        final Graphics2D g =(Graphics2D) buffer.getDrawGraphics();
        
        DibujoDebug.reiniciarContadorObtetos();
        
        DibujoDebug.dibujarRectanguloRelleno(g, 0, 0, Constantes.ANCHO_PANTALLA_COMPLETA, Constantes.ALTO_PANTALLA_COMPLETA, Color.black);
        //g.setColor(Color.black);
        //g.fillRect(0, 0, Constantes.ANCHO_PANTALLA_COMPLETA, Constantes.ALTO_PANTALLA_COMPLETA);
        g.setFont(Constantes.FUENTE_PIXEL);
        if (Constantes.FACTOR_ESCALADO_X != 1.0 || Constantes.FACTOR_ESCALADO_Y != 1.0) {
            g.scale(Constantes.FACTOR_ESCALADO_X, Constantes.FACTOR_ESCALADO_Y);
        }
        
        ge.dibujar(g);
        
        //debug aps y fps
        
        DibujoDebug.dibujarString(g, " APS: " + GestorPrincipal.obtenerAPS() + " FPS: " + GestorPrincipal.obtenerFPS() , 20, 20, Color.white);
        
        DatosDebug.enviarDato("Escalado X: " + Constantes.FACTOR_ESCALADO_X + " Escalado Y: " + Constantes.FACTOR_ESCALADO_Y);
        DatosDebug.enviarDato("OPF: " + DibujoDebug.obtenerContadorObjetos());
        raton.dibujar(g);
        
        if (GestorControles.teclado.debug) {
            DatosDebug.dibujarDatos(g);
        } else {
            DatosDebug.vaciarDatos();
        }
        
        Toolkit.getDefaultToolkit().sync();
        g.dispose();
        buffer.show();
    }
   
    public void actualizar () {
        raton.actualizar(this);
    }
    
    public int obtenerAncho () {
        return ancho;
    }

    public int obtenerAlto () {
        return alto;
    }

    public Raton obtenerRaton() {
        return raton;
    }

    
    
    
}

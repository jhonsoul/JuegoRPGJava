
package principal.inventario;

import principal.inventario.armas.Desarmado;
import principal.inventario.armas.Pistola;
import principal.inventario.cosumibles.Consumible;

public class RegistroObjetos {
    
    public static Objeto obtenerObjeto(final int idObjeto){
        Objeto objeto = null;
        //o - 499 objetos consumubles
        switch (idObjeto) {
            case 0:
                objeto = new Consumible(idObjeto, "Manzana roja", "");
                break;
            case 1:
                objeto = new Consumible(idObjeto, "Manzana amarilla", "");
                break;
            case 2:
                objeto = new Consumible(idObjeto, "Zanahoria", "");
                break;
            case 3:
                objeto = new Consumible(idObjeto, "Galleta", "");
                break;
            case 4:
                objeto = new Consumible(idObjeto, "Limon rojizo", "");
                break;
            case 5:
                objeto = new Consumible(idObjeto, "Fruta verde", "");
                break;
                
            //500 - 599 armas
                
            case 500:
                objeto = new Pistola(idObjeto, "Black Pistol", "", 2, 4, false, true, 0.4);
                break;
            case 599:
                objeto = new Desarmado(idObjeto, "Desarmado", "", 0, 0, false, false, 0.0);
                break;
        }
        
        
        return objeto;
    }

}

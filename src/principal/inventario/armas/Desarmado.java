
package principal.inventario.armas;

import java.awt.Rectangle;
import java.util.ArrayList;
import principal.entes.jugador.Jugador;

public class Desarmado extends Arma{


    public Desarmado(int id, String nombre, String descripcion, int ataqueMinimo, int ataqueMaximo, boolean automatica, boolean penetrante, double ataquesPorSegundo) {
		super(id, nombre, descripcion, ataqueMinimo, ataqueMaximo, automatica, penetrante, ataquesPorSegundo, "/sonidos/punch.wav");
	}

    @Override
    public ArrayList<Rectangle> obtenerAlcance(final Jugador jugador) {
        return null;
    }
    
}

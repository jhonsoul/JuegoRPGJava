
package principal;

import principal.control.GestorControles;
import principal.graficos.SuperficieDibujo;
import principal.graficos.Ventana;
import principal.maquinaestados.GestorEstados;
import principal.sonidos.Sonido;



public class GestorPrincipal {
    
    private boolean enFuncionamiento = false;
    private String titulo;
    private int ancho;
    private int alto;
    
    public static SuperficieDibujo sd;
    private Ventana ventana;
    private GestorEstados ge;
    
    private static int fps = 0;
    private static int aps = 0;
    
    private Sonido musica = new Sonido("/sonidos/AxleTheCuriousRoeFondo.wav");
    
    //Constructor
    private GestorPrincipal(final String titulo, final int ancho, final int alto){
        this.titulo = titulo;
        this.ancho = ancho;
        this.alto = alto;
    }
    
    //Metodo Principal
    public static void main(String[] args) {
        //System.setProperty("sun.java2d.opengl", "true");
        //System.setProperty("sun.java2d.d3d", "True");
	//System.setProperty("sun.java2d.ddforcevram", "True");
        GestorPrincipal gp = new GestorPrincipal("Parasito T", Constantes.ANCHO_PANTALLA_COMPLETA, Constantes.ALTO_PANTALLA_COMPLETA);
        
        gp.iniciarJuego();
        gp.iniciarBuclePrincipal();
    }

    //metodos
    private void iniciarJuego() {
        enFuncionamiento = true;
        inicializar();
        musica.repetir();
    }

    private void inicializar() {
        sd = new SuperficieDibujo(ancho, alto);
        ventana = new Ventana(titulo, sd);
        ge = new GestorEstados(sd);
    }
    
    private void iniciarBuclePrincipal() {
        int ActualizacionesAcumuladas = 0;
        int framesAcumulados = 0;
        final int NS_POR_SEGUNDO = 1000000000; // NS = nano segundos (representa cuantos nano segundos hay en un segundo)
        final int APS_OBJETIVO = 60; // APS = actualizaciones por segundo (framesAcumulados)
        final double NS_POR_ACTUALIZACION = (double)NS_POR_SEGUNDO / (double)APS_OBJETIVO;
        long referenciaActualizacion = System.nanoTime();
        long referenciaContador = System.nanoTime();
        double tiempoTranscurrido;
        double delta = 0; //Cantidad de tiempo que se realiza una actualización
                
        while (enFuncionamiento) {
            final long inicioBucle = System.nanoTime();
            
            tiempoTranscurrido = inicioBucle - referenciaActualizacion;
            referenciaActualizacion = inicioBucle;
            
            delta += tiempoTranscurrido / NS_POR_ACTUALIZACION;
            
            while (delta >= 1) {
                actualizar();
                ActualizacionesAcumuladas++;
                delta--;
            }
          
            dibujar();
            framesAcumulados++;
            
            if(System.nanoTime() - referenciaContador > NS_POR_SEGUNDO){
                
                aps = ActualizacionesAcumuladas;
                fps = framesAcumulados;
                ActualizacionesAcumuladas = 0;
                framesAcumulados = 0;
                referenciaContador = System.nanoTime();
            }
        }
    }

    private void actualizar() {
        if(GestorControles.teclado.inventarioActivo){
            ge.cambiarEstadoActual(1);
        } else {
            ge.cambiarEstadoActual(0);
        }
        ge.actualizar();
        sd.actualizar();
    }

    private void dibujar() {
        sd.dibujar(ge);
    }

    public static int obtenerFPS() {
        return fps;
    }

    public static int obtenerAPS() {
        return aps;
    }
}

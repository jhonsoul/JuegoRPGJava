
package principal.maquinaestados;

import java.awt.Graphics;
import principal.graficos.SuperficieDibujo;
import principal.maquinaestados.estados.juego.GestorJuego;
import principal.maquinaestados.estados.menujuego.GestorMenu;

public class GestorEstados {
    
    private EstadoJuego[] estados;
    private EstadoJuego estadosActual;
    
    public GestorEstados(final SuperficieDibujo sd){
        iniciarEstados(sd);
        iniciarEstadoActual();
    }

    private void iniciarEstados(final SuperficieDibujo sd) {
        estados = new EstadoJuego[2];
        estados[0] = new GestorJuego();
        estados[1] = new GestorMenu(sd);
        //Anadir e iniciar los demás estados
    }

    private void iniciarEstadoActual() {
        estadosActual = estados[0];
    }
    
    public void actualizar(){
        estadosActual.actualizar();
        
    }
    
    public void dibujar(final Graphics g){
        estadosActual.dibujar(g);
    }
    
    public void cambiarEstadoActual(final int nuevoEstado){
        estadosActual = estados[nuevoEstado];
    }
    
    public EstadoJuego obtenerEstadoActual(){
        return estadosActual;
    }
}

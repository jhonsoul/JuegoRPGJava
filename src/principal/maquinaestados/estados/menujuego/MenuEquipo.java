
package principal.maquinaestados.estados.menujuego;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import principal.Constantes;
import principal.ElementosPrincipales;
import principal.GestorPrincipal;
import principal.graficos.SuperficieDibujo;
import principal.herramientas.DibujoDebug;
import principal.herramientas.EscaladorElementos;
import principal.herramientas.GeneradorTooltip;
import principal.herramientas.MedidorString;
import principal.inventario.Objeto;
import principal.inventario.armas.Arma;
import principal.inventario.armas.Desarmado;

public class MenuEquipo extends SeccionMenu{

    final Rectangle panelObjetos = new Rectangle(ElementosPrincipales.estructuraMenu.FONDO.x + margenGeneral, barraPeso.y + barraPeso.height + margenGeneral, 248, Constantes.ALTO_JUEGO - barraPeso.y - barraPeso.height - margenGeneral * 2);
    final Rectangle panelEquipo = new Rectangle(panelObjetos.x + panelObjetos.width + margenGeneral, panelObjetos.y, 88, panelObjetos.height);
    final Rectangle panelAtributos = new Rectangle(panelEquipo.x + panelEquipo.width + margenGeneral, panelEquipo.y, 128, panelEquipo.height);
    
    final Rectangle titularPanelObjetos = new Rectangle(panelObjetos.x, panelObjetos.y, panelObjetos.width, 24);
    final Rectangle titularPanelEquipo = new Rectangle(panelEquipo.x, panelEquipo.y, panelEquipo.width, 24);
    final Rectangle titularPanelAtributos = new Rectangle(panelAtributos.x, panelAtributos.y, panelAtributos.width, 24);
    
    final Rectangle etiquetaArma = new Rectangle(titularPanelEquipo.x + margenGeneral, titularPanelEquipo.y + titularPanelEquipo.height + margenGeneral, titularPanelEquipo.width - margenGeneral * 2, margenGeneral * 2 + MedidorString.medirAltoPixeles(GestorPrincipal.sd.getGraphics(), "Arma")); 
    final Rectangle contenedorArma = new Rectangle(etiquetaArma.x + 1, etiquetaArma.y + etiquetaArma.height, etiquetaArma.width - 1, Constantes.LADO_SPRITE);
    
    Objeto objetoSeleccionado = null;
    /*
    final Rectangle etiquetaChaleco = new Rectangle(contenedorArma.x + margenGeneral, titularPanelEquipo.y + titularPanelEquipo.height + margenGeneral * 2 + contenedorArma.y + etiquetaArma.y, titularPanelEquipo.width - margenGeneral * 2, margenGeneral * 2 + MedidorString.medirAltoPixeles(GestorPrincipal.sd.getGraphics(), "Chaleco")); 
    final Rectangle contenedorChaleco = new Rectangle(etiquetaChaleco.x + 1, etiquetaChaleco.y + etiquetaChaleco.height, etiquetaChaleco.width - 1, Constantes.LADO_SPRITE);
    */

    
    public MenuEquipo(String nombreSeccion, Rectangle etiquetaMenu) {
        super(nombreSeccion, etiquetaMenu);
    }

    @Override
    public void actualizar() {
        actualizarPosicionesMenu();
        actualizarSeleccionRaton();
        actualizarObjetoSeleccionado();
    }

    @Override
    public void dibujar(Graphics g, SuperficieDibujo sd) {
        dibujarPaneles(g);
        dibujarLimitePeso(g);
        if (sd.obtenerRaton().obtenerRectanguloPosicion().intersects(EscaladorElementos.escalarRectanguloArriba(barraPeso))) {
            GeneradorTooltip.dibujarTooltip(g, sd, ElementosPrincipales.jugador.pesoActual + " / " + ElementosPrincipales.jugador.limitePeso);
        }
    }

    private void actualizarPosicionesMenu() {
        if (ElementosPrincipales.inventario.obtenerArmas().isEmpty()) {
            return;
        }
        
        for (int i = 0; i < ElementosPrincipales.inventario.obtenerArmas().size(); i++) {
            final Point puntoInicial = new Point(titularPanelObjetos.x + margenGeneral, titularPanelObjetos.y + titularPanelObjetos.height + margenGeneral);
            int idActual = ElementosPrincipales.inventario.obtenerArmas().get(i).obtenerId();
            
            ElementosPrincipales.inventario.obtenerObjeto(idActual).establecerPosicionMenu(new Rectangle(puntoInicial.x + i * (Constantes.LADO_SPRITE + margenGeneral), puntoInicial.y, Constantes.LADO_SPRITE, Constantes.LADO_SPRITE));
        }
    }
    
    private void dibujarPaneles(final Graphics g){
        dibujarPanelObjetos(g, panelObjetos, titularPanelObjetos, "Equipables");
        dibujarPanelEquipo(g, panelEquipo, titularPanelEquipo, "Equipo");
        dibujarPanelAtributos(g, panelAtributos, titularPanelAtributos, "Atributos");
    }
    
    private void dibujarElementosEquipables(final Graphics g, final Rectangle panelObjetos, final Rectangle titularPanel) {
        if (ElementosPrincipales.inventario.obtenerArmas().isEmpty()) {
            return;
        }
        final Point puntoInicial = new Point(titularPanel.x + margenGeneral, titularPanel.y + titularPanel.height + margenGeneral);
        
        for (int i = 0; i < ElementosPrincipales.inventario.obtenerArmas().size(); i++) {
            int idActual = ElementosPrincipales.inventario.obtenerArmas().get(i).obtenerId();
            Objeto objectoActual = ElementosPrincipales.inventario.obtenerObjeto(idActual);
            
            DibujoDebug.dibujarImagenes(g, objectoActual.obtenerSprite().obtenerImagen(), objectoActual.obtenerPosicionMenu().x, objectoActual.obtenerPosicionMenu().y);
            DibujoDebug.dibujarRectanguloRelleno(g, puntoInicial.x + i * (Constantes.LADO_SPRITE) + Constantes.LADO_SPRITE - 12, puntoInicial.y + Constantes.LADO_SPRITE - 8, 12, 8, Color.black);
            
            String texto = "";
            if (objectoActual.obtenerCantidad() < 10) {
                texto = "0" + objectoActual.obtenerCantidad();
            } else {
                texto += objectoActual.obtenerCantidad();
            }
            g.setFont(g.getFont().deriveFont(10f));
            DibujoDebug.dibujarString(g, texto, puntoInicial.x + i * (Constantes.LADO_SPRITE + margenGeneral) + Constantes.LADO_SPRITE - MedidorString.medirAnchoPixeles(g, texto), puntoInicial.y + 31, Color.white);
        }
        g.setFont(g.getFont().deriveFont(12f));
        
        if (objetoSeleccionado != null) {
            DibujoDebug.dibujarImagenes(g, objetoSeleccionado.obtenerSprite().obtenerImagen(), new Point(objetoSeleccionado.obtenerPosicionFlotante().x, objetoSeleccionado.obtenerPosicionFlotante().y));
        }
    }
    
    private void dibujarPanelObjetos(final Graphics g, final Rectangle panel, final Rectangle titularPanel, final String nombrePanel) {
        dibujarPanel(g, panel, titularPanel, nombrePanel);
        //Dibujar objetos
        dibujarElementosEquipables(g, panel, titularPanel);
    }
    
    private void dibujarPanelEquipo(final Graphics g, final Rectangle panel, final Rectangle titularPanel, final String nombrePanel) {
        dibujarPanel(g, panel, titularPanel, nombrePanel);
        //Dibujar ranuras equipables
        DibujoDebug.dibujarRectanguloRelleno(g, etiquetaArma, Color.black);
        DibujoDebug.dibujarContornoRectangulo(g, contenedorArma, Color.black);
        
        if (!(ElementosPrincipales.jugador.obtenerAlmacenEquipo().obtenerArma1() instanceof  Desarmado)) {
            Point coordenadaImagen = new Point(contenedorArma.x + contenedorArma.width / 2 - Constantes.LADO_SPRITE / 2, contenedorArma.y);
            DibujoDebug.dibujarImagenes(g, ElementosPrincipales.jugador.obtenerAlmacenEquipo().obtenerArma1().obtenerSprite().obtenerImagen(), coordenadaImagen);
        }
        //Dibujar arma equipada
        DibujoDebug.dibujarString(g, "Arma", new Point(etiquetaArma.x + etiquetaArma.width / 2 - MedidorString.medirAnchoPixeles(g, "Arma") / 2, etiquetaArma.y + etiquetaArma.height / 2 + MedidorString.medirAltoPixeles(g, "Arma") / 2), Color.white);
        
        
        /*
        DibujoDebug.dibujarRectanguloRelleno(g, etiquetaChaleco, Color.black);
        DibujoDebug.dibujarContornoRectangulo(g, contenedorChaleco, Color.black);
        DibujoDebug.dibujarString(g, "Chaleco", new Point(etiquetaChaleco.x + etiquetaChaleco.width / 2 - MedidorString.medirAnchoPixeles(g, "Chaleco") / 2, etiquetaChaleco.y + etiquetaChaleco.height / 2 + MedidorString.medirAltoPixeles(g, "Chaleco") / 2), Color.white);
        */
    }
    
    private void dibujarPanelAtributos(final Graphics g, final Rectangle panel, final Rectangle titularPanel, final String nombrePanel) {
        dibujarPanel(g, panel, titularPanel, nombrePanel);
        //Dibujar los atributos
    }
    
    private void dibujarPanel(final Graphics g, final Rectangle panel, final Rectangle titular, final String nombrePanel ){
        DibujoDebug.dibujarContornoRectangulo(g, panel, Constantes.COLOR_MENU_BASE);
        DibujoDebug.dibujarRectanguloRelleno(g, titular);
        DibujoDebug.dibujarString(g, nombrePanel, new Point(titular.x + titular.width / 2 - MedidorString.medirAnchoPixeles(g, nombrePanel) / 2, titular.y + titular.height - MedidorString.medirAltoPixeles(g, nombrePanel) / 2 - 4), Color.white);
    }

    private void actualizarSeleccionRaton() {
        Rectangle posicionRaton = GestorPrincipal.sd.obtenerRaton().obtenerRectanguloPosicion();
        if (posicionRaton.intersects(EscaladorElementos.escalarRectanguloArriba(panelObjetos))) {
            if (ElementosPrincipales.inventario.obtenerArmas().isEmpty()) {
                return;
            }
            for (Objeto objeto : ElementosPrincipales.inventario.obtenerArmas()) {
                if (GestorPrincipal.sd.obtenerRaton().obtenerClick() && posicionRaton.intersects(EscaladorElementos.escalarRectanguloArriba(objeto.obtenerPosicionMenu()))) {
                    objetoSeleccionado = objeto;
                }
            }
        } else if (posicionRaton.intersects(EscaladorElementos.escalarRectanguloArriba(panelEquipo))) {
            if (objetoSeleccionado != null && objetoSeleccionado instanceof Arma && GestorPrincipal.sd.obtenerRaton().obtenerClick() && posicionRaton.intersects(EscaladorElementos.escalarRectanguloArriba(contenedorArma))) {
                ElementosPrincipales.jugador.obtenerAlmacenEquipo().cambiarArma1((Arma)objetoSeleccionado);
                objetoSeleccionado = null;
            }
        } else if (posicionRaton.intersects(EscaladorElementos.escalarRectanguloArriba(panelAtributos))) {
            
        }
    }
    
    private void actualizarObjetoSeleccionado() {
        if (objetoSeleccionado != null) {
            if (GestorPrincipal.sd.obtenerRaton().obtenerClickDerecho()) {
                objetoSeleccionado = null;
                return;
            }
            Point posicionRaton = EscaladorElementos.escalarPuntoAbajo(GestorPrincipal.sd.obtenerRaton().obtenerPuntoPosicion());
            objetoSeleccionado.establecerPosicionFlotante(new Rectangle(posicionRaton.x, posicionRaton.y, Constantes.LADO_SPRITE, Constantes.LADO_SPRITE));
        }
    }
    
    public void eliminarObjetoSelecionado() {
        objetoSeleccionado = null;
    }
}


package principal.maquinaestados.estados.menujuego;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import principal.Constantes;
import principal.ElementosPrincipales;
import principal.graficos.SuperficieDibujo;
import principal.herramientas.DibujoDebug;

public abstract class SeccionMenu {
    
    protected final String nombreSeccion;
    protected final Rectangle etiquetaMenu;
    private final EstructuraMenu em;
    protected final int margenGeneral = 8;
    private static final Color COLOR_INACTIVO = new Color(0x2a2a2a);
    protected final Rectangle barraPeso;
    
    public SeccionMenu(final String nombreSeccion, final Rectangle etiquetaMenu) {
        this.nombreSeccion = nombreSeccion;
        this.etiquetaMenu = etiquetaMenu;
        em = new EstructuraMenu();
        int anchoBarra = 100;
        barraPeso = new Rectangle(Constantes.ANCHO_JUEGO - anchoBarra - 12, em.BANNER_SUPUERIOR.height + margenGeneral, ElementosPrincipales.jugador.limitePeso, margenGeneral);
    }
    
    public abstract void actualizar();
    
    public abstract  void dibujar(final Graphics g, final SuperficieDibujo sd);
    
    public void dibujarEtiquetaInactiva(final Graphics g) {
        DibujoDebug.dibujarRectanguloRelleno(g, etiquetaMenu, Color.white);
        DibujoDebug.dibujarString(g, nombreSeccion, etiquetaMenu.x + 15, etiquetaMenu.y + 12, Color.black);
    }
    public void dibujarEtiquetaActiva(final Graphics g) {
        DibujoDebug.dibujarRectanguloRelleno(g, etiquetaMenu, Color.white);
        final Rectangle marcaActiva = new Rectangle(etiquetaMenu.x, etiquetaMenu.y, 5, etiquetaMenu.height);
        
        DibujoDebug.dibujarRectanguloRelleno(g, marcaActiva, Constantes.COLOR_MENU_BASE);
        DibujoDebug.dibujarString(g, nombreSeccion, etiquetaMenu.x + 15, etiquetaMenu.y + 12, Color.black);
    }
    
    public void dibujarEtiquetaInactivaResaltada(final Graphics g){
        DibujoDebug.dibujarRectanguloRelleno(g, etiquetaMenu, Color.white);
        DibujoDebug.dibujarRectanguloRelleno(g, new Rectangle(etiquetaMenu.x + etiquetaMenu.width - 10, etiquetaMenu.y + 5, 5, etiquetaMenu.height - 10), COLOR_INACTIVO);
        DibujoDebug.dibujarString(g, nombreSeccion, etiquetaMenu.x + 15, etiquetaMenu.y + 12, Color.black);
    }
    
    public void dibujarEtiquetaActivaResaltada(final Graphics g){
        DibujoDebug.dibujarRectanguloRelleno(g, etiquetaMenu, Color.white);
        final Rectangle marcaActiva = new Rectangle(etiquetaMenu.x, etiquetaMenu.y, 5, etiquetaMenu.height);
        DibujoDebug.dibujarRectanguloRelleno(g, marcaActiva, Constantes.COLOR_MENU_BASE);
        DibujoDebug.dibujarString(g, nombreSeccion, etiquetaMenu.x + 15, etiquetaMenu.y + 12, Color.black);
        
        DibujoDebug.dibujarRectanguloRelleno(g, new Rectangle(etiquetaMenu.x + etiquetaMenu.width - 10, etiquetaMenu.y + 5, 5, etiquetaMenu.height - 10), COLOR_INACTIVO);
        DibujoDebug.dibujarString(g, nombreSeccion, etiquetaMenu.x + 15, etiquetaMenu.y + 12, Color.black);
    }
    
    protected void dibujarLimitePeso(final Graphics g) {
        final Rectangle contenidoBarra = new Rectangle(barraPeso.x + 1, barraPeso.y + 1, barraPeso.width / (ElementosPrincipales.jugador.limitePeso / ElementosPrincipales.jugador.pesoActual), barraPeso.height - 2);
        DibujoDebug.dibujarString(g, "Peso ",barraPeso.x -30, barraPeso.y + 7, Color.BLACK);
        DibujoDebug.dibujarRectanguloRelleno(g, barraPeso, Color.gray);
        DibujoDebug.dibujarRectanguloRelleno(g, contenidoBarra, Constantes.COLOR_MENU_BASE);
    }
    
    public String obtenerNombreSeccion() {
        return nombreSeccion;
    }

    public Rectangle obtenerEtiquetaMenu() {
        return etiquetaMenu;
    }
    
    public Rectangle obtenerEtiquetaMenuEscalado() {
        final Rectangle etiquetaEscalada = new Rectangle((int)(etiquetaMenu.x * Constantes.FACTOR_ESCALADO_X), (int)(etiquetaMenu.y * Constantes.FACTOR_ESCALADO_Y), (int)(etiquetaMenu.width * Constantes.FACTOR_ESCALADO_X), (int)(etiquetaMenu.height * Constantes.FACTOR_ESCALADO_Y));
        return etiquetaEscalada;
    }
}

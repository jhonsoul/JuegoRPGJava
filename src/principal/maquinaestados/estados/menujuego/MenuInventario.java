
package principal.maquinaestados.estados.menujuego;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import principal.Constantes;
import principal.ElementosPrincipales;
import principal.graficos.SuperficieDibujo;
import principal.herramientas.DibujoDebug;
import principal.herramientas.EscaladorElementos;
import principal.herramientas.GeneradorTooltip;
import principal.herramientas.MedidorString;
import principal.inventario.Objeto;

public class MenuInventario extends SeccionMenu{

        
    private final EstructuraMenu em;
    
    public MenuInventario(String nombreSeccion, Rectangle etiquetaMenu, EstructuraMenu em) {
        super(nombreSeccion, etiquetaMenu);
        
        this.em = em;
        
    }

    @Override
    public void actualizar() {
        actualizarPosicionesMenu();
    }

    @Override
    public void dibujar(Graphics g, SuperficieDibujo sd) {
        
        dibujarElementosConsumibles(g, em);
        dibujarPaginador(g, em);
        dibujarLimitePeso(g);
        if (sd.obtenerRaton().obtenerRectanguloPosicion().intersects(EscaladorElementos.escalarRectanguloArriba(barraPeso))) {
            GeneradorTooltip.dibujarTooltip(g, sd, ElementosPrincipales.jugador.pesoActual + " / " + ElementosPrincipales.jugador.limitePeso);
        }
    }
    
    private void dibujarElementosConsumibles(final Graphics g, EstructuraMenu em) {
        if (ElementosPrincipales.inventario.obtenerConsumibles().isEmpty()) {
            return;
        }
        final Point puntoInicial = new Point(em.FONDO.x + 16, barraPeso.y + barraPeso.height + margenGeneral);
        for (int i = 0; i < ElementosPrincipales.inventario.obtenerConsumibles().size(); i++) {
            int idActual = ElementosPrincipales.inventario.obtenerConsumibles().get(i).obtenerId();
            Objeto objectoActual = ElementosPrincipales.inventario.obtenerObjeto(idActual);
            
            DibujoDebug.dibujarImagenes(g, objectoActual.obtenerSprite().obtenerImagen(), objectoActual.obtenerPosicionMenu().x, objectoActual.obtenerPosicionMenu().y);
            DibujoDebug.dibujarRectanguloRelleno(g, puntoInicial.x + i * (Constantes.LADO_SPRITE + margenGeneral) + Constantes.LADO_SPRITE - 12, puntoInicial.y + Constantes.LADO_SPRITE - 8, 12, 8, Color.black);
            String texto = "";
            if (objectoActual.obtenerCantidad() < 10) {
                texto = "0" + objectoActual.obtenerCantidad();
            } else {
                texto += objectoActual.obtenerCantidad();
            }
            g.setFont(g.getFont().deriveFont(10f));
            DibujoDebug.dibujarString(g, texto, puntoInicial.x + i * (Constantes.LADO_SPRITE + margenGeneral) + Constantes.LADO_SPRITE - MedidorString.medirAnchoPixeles(g, texto), puntoInicial.y + Constantes.LADO_SPRITE - 1, Color.white);
        }
        g.setFont(g.getFont().deriveFont(12f));
    }

    
    private void dibujarPaginador(final Graphics g, EstructuraMenu em) {
        final int anchoBoton = Constantes.LADO_SPRITE;
        final int altoBoton = Constantes.LADO_SPRITE / 2;
        
        final Rectangle anterior = new Rectangle(em.FONDO.x + em.FONDO.width - margenGeneral * 2 - anchoBoton * 2 - 4, em.FONDO.y + em.FONDO.height - margenGeneral - altoBoton, anchoBoton, altoBoton);
        final Rectangle siguiente = new Rectangle(anterior.x + anterior.width + margenGeneral, anterior.y, anchoBoton, altoBoton);
        
        DibujoDebug.dibujarRectanguloRelleno(g, anterior, Constantes.COLOR_MENU_BASE);
        DibujoDebug.dibujarRectanguloRelleno(g, siguiente, Constantes.COLOR_MENU_BASE);
    }
    
    private void actualizarPosicionesMenu() {
        if (ElementosPrincipales.inventario.obtenerConsumibles().isEmpty()) {
            return;
        }
        
        for (int i = 0; i < ElementosPrincipales.inventario.obtenerConsumibles().size(); i++) {
            final Point puntoInicial = new Point(em.FONDO.x + 16, barraPeso.y + barraPeso.height + margenGeneral);
            int idActual = ElementosPrincipales.inventario.obtenerConsumibles().get(i).obtenerId();
            
            ElementosPrincipales.inventario.obtenerObjeto(idActual).establecerPosicionMenu(new Rectangle(puntoInicial.x + i * (Constantes.LADO_SPRITE + margenGeneral), puntoInicial.y, Constantes.LADO_SPRITE, Constantes.LADO_SPRITE));
        }
    }
}

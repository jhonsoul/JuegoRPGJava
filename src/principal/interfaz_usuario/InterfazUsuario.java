
package principal.interfaz_usuario;

import java.awt.Color;
import java.awt.Graphics;
import principal.herramientas.DibujoDebug;

public class InterfazUsuario {
    
    public static void dibujarBarraResistencia (Graphics g, int resistencia) {
        int ancho = 100 * resistencia / 600;
        Color rojoOscuro = new Color(200, 0, 0);
        
        //Barra resistencia
        DibujoDebug.dibujarContornoRectangulo(g, 19, 249, 101, 17, Color.white);
        DibujoDebug.dibujarRectanguloRelleno(g, 20, 250, ancho, 5, Color.red);
        DibujoDebug.dibujarRectanguloRelleno(g, 20, 255, ancho, 10, rojoOscuro);
        
        
    }
}

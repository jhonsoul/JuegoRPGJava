
package principal.interfaz_usuario;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import principal.Constantes;
import principal.ElementosPrincipales;
import principal.entes.jugador.datos.DatoJugadorExperiencia;
import principal.entes.jugador.datos.DatoJugadorVitalidad;
import principal.graficos.SuperficieDibujo;
import principal.herramientas.DatosDebug;
import principal.herramientas.DibujoDebug;
import principal.herramientas.GeneradorTooltip;

public class MenuInferior {
    
    private Rectangle areaInventario;
    private Rectangle bordeAreaInventario;
    
    private Color rojoClaro; 
    private Color azulClaro;
    private Color verdeClaro; 
    private Color amarilloClaro; 
    
    private static final int MEDIDA_VERTICAL = 4;
    private static final int ANCHO_TOTAL_BARRA = 100;
    
    public MenuInferior() {
        
        final int altoMenu = Constantes.LADO_SPRITE * 2;
        areaInventario = new Rectangle(0, Constantes.ALTO_JUEGO - altoMenu, Constantes.ANCHO_JUEGO, altoMenu);
        bordeAreaInventario = new Rectangle(areaInventario.x, areaInventario.y -1, areaInventario.width, 1);
        
        rojoClaro = new Color(250, 50, 50);
        azulClaro = new Color(0, 50, 255);
        verdeClaro = new Color(100, 250, 100);
        amarilloClaro = new Color(250, 250, 50);
    }
    
    public void dibujar(final Graphics g) {
        dibujarAreaInventario(g);
        dibujarBarraVitalidad(g);
        dibujarBarraPoder(g);
        dibujarBarraResistencia(g);
        dibujarBarraExperiencia(g);
        dibujarNivel(g);
        dibujarRanurasObjetos(g);
    }

    private void dibujarAreaInventario(final Graphics g) {
        DibujoDebug.dibujarRectanguloRelleno(g, areaInventario, Constantes.COLOR_MENU_BASE);
        DibujoDebug.dibujarRectanguloRelleno(g, bordeAreaInventario, Color.white);
    }
    
    private void dibujarBarraVitalidad(final Graphics g) {
       
        DibujoDebug.dibujarRectanguloRelleno(g, areaInventario.x + Constantes.LADO_SPRITE , areaInventario.y + MEDIDA_VERTICAL, DatoJugadorVitalidad.vidaActualPorcential(), MEDIDA_VERTICAL, rojoClaro);
        DibujoDebug.dibujarRectanguloRelleno(g, areaInventario.x + Constantes.LADO_SPRITE , areaInventario.y + MEDIDA_VERTICAL * 2, DatoJugadorVitalidad.vidaActualPorcential(), MEDIDA_VERTICAL, Color.red);
        DibujoDebug.dibujarString(g, "VIT", areaInventario.x + 7, areaInventario.y + MEDIDA_VERTICAL * 3, Color.white);
        DibujoDebug.dibujarString(g, "" + DatoJugadorVitalidad.obtenerVidaActual(), ANCHO_TOTAL_BARRA + 45, areaInventario.y + MEDIDA_VERTICAL * 3, Color.white);
    }
    
    private void dibujarBarraPoder(final Graphics g) {
        
        DibujoDebug.dibujarRectanguloRelleno(g, areaInventario.x + Constantes.LADO_SPRITE , areaInventario.y + MEDIDA_VERTICAL * 4, ANCHO_TOTAL_BARRA, MEDIDA_VERTICAL, azulClaro);
        DibujoDebug.dibujarRectanguloRelleno(g, areaInventario.x + Constantes.LADO_SPRITE , areaInventario.y + MEDIDA_VERTICAL * 5, ANCHO_TOTAL_BARRA, MEDIDA_VERTICAL, Color.blue);
        DibujoDebug.dibujarString(g, "POW", areaInventario.x + 7, areaInventario.y + MEDIDA_VERTICAL * 6, Color.white);
        DibujoDebug.dibujarString(g, "500", ANCHO_TOTAL_BARRA + 45, areaInventario.y + MEDIDA_VERTICAL * 6, Color.white);
    }
    
    private void dibujarBarraResistencia(final Graphics g) {
        final int anchoBarra = ANCHO_TOTAL_BARRA * ElementosPrincipales.jugador.obtenerResistencia() / 600;
        
        DibujoDebug.dibujarRectanguloRelleno(g, areaInventario.x + Constantes.LADO_SPRITE , areaInventario.y + MEDIDA_VERTICAL * 7, anchoBarra, MEDIDA_VERTICAL, verdeClaro);
        DibujoDebug.dibujarRectanguloRelleno(g, areaInventario.x + Constantes.LADO_SPRITE , areaInventario.y + MEDIDA_VERTICAL * 8, anchoBarra, MEDIDA_VERTICAL, Color.green);
        DibujoDebug.dibujarString(g, "RES", areaInventario.x + 7, areaInventario.y + MEDIDA_VERTICAL * 9, Color.white);
        DibujoDebug.dibujarString(g, "" + ElementosPrincipales.jugador.obtenerResistencia(), ANCHO_TOTAL_BARRA + 45, areaInventario.y + MEDIDA_VERTICAL * 9, Color.white);
    }
    
    private void dibujarBarraExperiencia(final Graphics g) {
        DibujoDebug.dibujarRectanguloRelleno(g, areaInventario.x + Constantes.LADO_SPRITE , areaInventario.y + MEDIDA_VERTICAL * 10, DatoJugadorExperiencia.obtenerExpericiaPorcentual(), MEDIDA_VERTICAL, amarilloClaro);
        DibujoDebug.dibujarRectanguloRelleno(g, areaInventario.x + Constantes.LADO_SPRITE , areaInventario.y + MEDIDA_VERTICAL * 11, DatoJugadorExperiencia.obtenerExpericiaPorcentual(), MEDIDA_VERTICAL, Color.yellow);
        DibujoDebug.dibujarString(g, "EXP", areaInventario.x + 7, areaInventario.y + MEDIDA_VERTICAL * 12, Color.white);
        DibujoDebug.dibujarString(g, DatoJugadorExperiencia.obtenerExpericiaPorcentual() + "%", ANCHO_TOTAL_BARRA + 45, areaInventario.y + MEDIDA_VERTICAL * 12, Color.white);
    }
    
    private void dibujarNivel(final Graphics g) {
        DibujoDebug.dibujarString(g, "Nivel "+DatoJugadorExperiencia.obtenerNivel(), areaInventario.x + 7, areaInventario.y + MEDIDA_VERTICAL * 15, Color.white);
    }
    
    private void dibujarRanurasObjetos(final Graphics g) {
        final int anchoRanura = Constantes.LADO_SPRITE;
        final int numeroRanuras = 10;
        final int espacidadoRanuras = 10;
        final int anchoTotal = anchoRanura * numeroRanuras + espacidadoRanuras * numeroRanuras;
        final int xInicial = Constantes.ANCHO_JUEGO - anchoTotal;
        final int anchoRanuraYEspaciado = anchoRanura + espacidadoRanuras;
                
        for (int i = 0; i < numeroRanuras; i++) {
            int xActual = xInicial + anchoRanuraYEspaciado * i;
            Rectangle ranura = new Rectangle(xActual, areaInventario.y + 4, anchoRanura, anchoRanura);
            DibujoDebug.dibujarRectanguloRelleno(g, ranura, Color.white);
            DibujoDebug.dibujarString(g, "" + (i + 1), xActual + 13, areaInventario.y + anchoRanura + 18, Color.white);
        }
    }
    
    public static int obteneranchoTotal() {
        return ANCHO_TOTAL_BARRA;
    }
}

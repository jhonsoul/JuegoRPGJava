
package principal.control;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.SwingUtilities;
import principal.Constantes;
import principal.graficos.SuperficieDibujo;
import principal.herramientas.CargadorRecursos;
import principal.herramientas.DatosDebug;

public class Raton extends MouseAdapter{
    
    private final Cursor cursor;
    private Point posicion;
    
    private boolean click;
    private boolean clickDerecho;
    
    public Raton(final SuperficieDibujo sd){
        Toolkit configuracion = Toolkit.getDefaultToolkit();
        
        BufferedImage icono = CargadorRecursos.cargarImagenCompatibleTranslucida(Constantes.RUTA_ICONO_RATON);
        Constantes.LADO_CURSOR = icono.getWidth();
        Point punta = new Point(16, 16);
        this.cursor = configuracion.createCustomCursor(icono, punta, "Cursor mira");
        
        posicion = new Point();
        actualizarPosicion(sd);
        
        click = false;
        clickDerecho = false;
    }
    
    public void actualizar (final SuperficieDibujo sd) {
        actualizarPosicion(sd);
    }
    
    public void dibujar(final Graphics g){
        DatosDebug.enviarDato("RX: " + posicion.x + " RY: " + posicion.y);
    }
    
    public Cursor obtenerCursor(){
        return this.cursor;
    }
    
    private void actualizarPosicion(final SuperficieDibujo sd){
        final Point posicionIncial = MouseInfo.getPointerInfo().getLocation();
        
        SwingUtilities.convertPointFromScreen(posicionIncial, sd);
        
        posicion.setLocation(posicionIncial.x, posicionIncial.y);
    }
    
    public Point obtenerPuntoPosicion() {
        return posicion;
    }
    
    public Rectangle obtenerRectanguloPosicion() {
        final Rectangle area = new Rectangle(posicion.x, posicion.y, 1, 1);
        return area;
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e)) {
            click = true;
        } else if (SwingUtilities.isRightMouseButton(e)){
            clickDerecho = true;
        }
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e)) {
            click = false;
        } else if (SwingUtilities.isRightMouseButton(e)){
            clickDerecho = false;
        }
    }
    
    public boolean obtenerClick() {
        return click;
    }

    public boolean obtenerClickDerecho() {
        return clickDerecho;
    }

}

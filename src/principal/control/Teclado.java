package principal.control;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Teclado implements KeyListener{
    
    public Tecla arriba = new Tecla();
    public Tecla abajo = new Tecla();
    public Tecla izquierda = new Tecla();
    public Tecla derecha = new Tecla();
    
    public boolean recogiendo = false;
    public boolean corriendo = false;
    public boolean debug = false;
    public boolean inventarioActivo = false;
    public boolean atacando = false;
    public boolean cerrar = true;
    
    
    @Override
    public void keyTyped(KeyEvent ke) {
    }   

    @Override
    public void keyPressed(KeyEvent ke) {
        switch(ke.getKeyCode()){
            case KeyEvent.VK_W:
                arriba.teclaPulsada();
                break;
            case KeyEvent.VK_S:
                abajo.teclaPulsada();
                break;
            case KeyEvent.VK_A:
                izquierda.teclaPulsada();
                break;
            case KeyEvent.VK_D:
                derecha.teclaPulsada();
                break;
            case KeyEvent.VK_SHIFT:
                corriendo = true;
                break;
            case KeyEvent.VK_F12:
                debug = !debug;
                break;
            case KeyEvent.VK_I:
                inventarioActivo = !inventarioActivo;
                break;
            case KeyEvent.VK_E:
                recogiendo = true;
                break;
            case KeyEvent.VK_SPACE:
                atacando = true;
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        switch(ke.getKeyCode()){
            case KeyEvent.VK_W:
                arriba.teclaLiberada();
                break;
            case KeyEvent.VK_S:
                abajo.teclaLiberada();
                break;
            case KeyEvent.VK_A:
                izquierda.teclaLiberada();
                break;
            case KeyEvent.VK_D:
                derecha.teclaLiberada();
                break;
            case KeyEvent.VK_SHIFT:
                corriendo = false;
                break;
            case KeyEvent.VK_ESCAPE:
                new Tecla().cerrarVentana(cerrar);
                cerrar = !cerrar;
                break;
            case KeyEvent.VK_E:
                recogiendo = false;
                break;
            case KeyEvent.VK_SPACE:
                atacando = false;
                break;        
        }
    }
}

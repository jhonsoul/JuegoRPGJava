
package principal.control;

import javax.swing.JOptionPane;

public class Tecla {
    private boolean pulsada = false;
    private long ultimaPulsacion = System.nanoTime();
    
    public void teclaPulsada(){
        pulsada = true;
        ultimaPulsacion = System.nanoTime();
    }
    
    public void teclaLiberada(){
        pulsada = false;
    }
    
    public boolean estaPulsada(){
        return  pulsada;
    }
    
    public long obtenerUltimaPulsacion(){
        return ultimaPulsacion;
    }
    
    public void cerrarVentana(boolean pulsada) {
        if (pulsada) {
            int teclaEsc = JOptionPane.showConfirmDialog(null, "¿Desea cerrar el juego?", "Confirmar cierre del juego", JOptionPane.YES_NO_OPTION); 
            if (teclaEsc == JOptionPane.YES_OPTION) {
                System.exit(0); 
            } 
        }
    }
}


package principal.entes.jugador.datos;

import principal.entes.Enemigos.Enemigo;

public class DatoJugadorExperiencia {

    private static int experiencia = 0;
    private static int expericenciaProximoNivel = 100;
    private static int nivel = 1;
    private static int expericienciaEnemigo = 0;
    
    

    public static void incrementarExperiencia() {
        expericienciaEnemigo = Enemigo.obtenerExperiencia();
        expericienciaEnemigo += experiencia;
        if (expericienciaEnemigo >= expericenciaProximoNivel) {
            while (expericienciaEnemigo >= expericenciaProximoNivel) {
                expericienciaEnemigo -= expericenciaProximoNivel;
                nivel ++;
                DatoJugadorVitalidad.aumentarVidaMaxima();
                expericenciaProximoNivel = nivel * 100;
            }
        }
        experiencia = expericienciaEnemigo;
    }
    
    public static int obtenerExpericiaPorcentual() {
        return (100 * experiencia / expericenciaProximoNivel);
    }
    
    public static int obtenerNivel() {
        return nivel;
    }

    public static int obtenerExperiencia() {
        return experiencia;
    }

    public static int obtenerExpericenciaProximoNivel() {
        return expericenciaProximoNivel;
    }
    
    
}

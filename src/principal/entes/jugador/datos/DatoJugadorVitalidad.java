package principal.entes.jugador.datos;

public class DatoJugadorVitalidad {
      
    private static int vidaMaxima = 100;
    private static int vidaActual = vidaMaxima;

    public static int obtenerVidaActual() {
        return vidaActual;
    }
    
    public static int vidaActualPorcential() {
        return (100 * vidaActual / vidaMaxima);
    }
    
    public static void aumentarVidaMaxima() {
        vidaMaxima += 50;
        vidaActual = vidaMaxima;
    }
    
    public static void quitarVidaActual(final int golpe) {
        vidaActual -= golpe;
        if (vidaActual < 0) {
            vidaActual = 0;
        }
    }
    
    public static void aumentarVidaActual(final int curacion) {
        vidaActual += curacion;
        if (vidaActual > vidaMaxima) {
            vidaActual = vidaMaxima;
        }
    }
}

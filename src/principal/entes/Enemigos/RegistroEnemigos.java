
package principal.entes.Enemigos;

import principal.entes.Enemigos.Enemigo;

public class RegistroEnemigos {
    
    public static Enemigo obtenerEnemigo(final int idEnemigo) {
        Enemigo enemigo = null;
        
        switch(idEnemigo) {
            case 1:
                enemigo = new Zombie(idEnemigo, "Zombie", 10, 100,"/sonidos/zombieRoar.wav");
                break;
        }
        return enemigo;
    }
}

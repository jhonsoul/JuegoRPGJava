package principal.entes.Enemigos;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import principal.Constantes;
import principal.ElementosPrincipales;
import principal.dijkstra.Nodo;
import principal.herramientas.CalculadoraDistancia;
import principal.herramientas.DibujoDebug;
import principal.sonidos.Sonido;

public class Enemigo {
    
    private final int idEnemigo;
    private double posicionX;
    private double posicionY;
    
    private final String nombre;
    private final int vidaMaxima;
    private float vidaActual;

    private static int exp;
    
    private  Nodo siguienteNodo;
    
    private Sonido lamento;
    private long duracionLamento;
    private long lamentoSiguiente;
    
    public Enemigo(final int idEnemigo, final String nombre, final int vidaMaxima, final int exp,final String rutaLamento) {
        this.idEnemigo = idEnemigo;
        this.nombre = nombre;
        this.vidaMaxima = vidaMaxima;
        
        this.posicionX = 0;
        this.posicionY = 0;
        this.vidaActual = vidaMaxima;
        
        this.exp = exp;
        
        this.lamento = new Sonido(rutaLamento);
        this.duracionLamento = lamento.obtenerDuracion();
    }
    
    public void actualizar(ArrayList<Enemigo> enemigos) {
        if (lamentoSiguiente > 0) {
            lamentoSiguiente -= 1000000 / 60;
        }
        moverHaciaSiguienteNodo(enemigos);
        
    }
    
    private void moverHaciaSiguienteNodo(ArrayList<Enemigo> enemigos) {
        if (siguienteNodo == null) {
            return;
        }
        
        for (Enemigo enemigo : enemigos) {
            if (enemigo.obtenerAreaPosicional().equals(this.obtenerAreaPosicional())) {
                continue;
            }
            if (enemigo.obtenerAreaPosicional().intersects(siguienteNodo.obtenerAreaPixeles())) {
                return;
            }
        }
        
        double velocidad = 0.5;
        
        int xSiguienteNodo = siguienteNodo.obtenerPosicion().x * Constantes.LADO_SPRITE;
        int ySiguienteNodo = siguienteNodo.obtenerPosicion().y * Constantes.LADO_SPRITE;
        
        if (posicionX < xSiguienteNodo) {
            posicionX += velocidad;
        }
        if (posicionX > xSiguienteNodo) {
            posicionX -= velocidad;
        }
        if (posicionY < ySiguienteNodo) {
            posicionY += velocidad;
        }
        if (posicionY > ySiguienteNodo) {
            posicionY -= velocidad;
        }
    }
    
    public void dibujar(final Graphics g, final int puntoX, final int puntoY) {
        if (vidaActual <= 0) {
            return;
        }
        dibujarBarravida(g, puntoX, puntoY);
        DibujoDebug.dibujarContornoRectangulo(g, obtenerArea());
        dibujarDistancia(g, puntoX, puntoY);
    }
    
    private void dibujarBarravida(final Graphics g, final int puntoX, final int puntoY) {
        DibujoDebug.dibujarRectanguloRelleno(g, puntoX, puntoY - 5, Constantes.LADO_SPRITE * (int)vidaActual / vidaMaxima, 2, Color.red);
    }
    
    private void dibujarDistancia(final Graphics g, final int puntoX, final int puntoY) {
        Point puntoJugador = new Point((int)ElementosPrincipales.jugador.obtenerPosicionX(), (int)ElementosPrincipales.jugador.obtenerPosicionY());
        Point puntoEnemigo = new Point((int)posicionX, (int)posicionY);
        Double distancia = CalculadoraDistancia.obtenerDistanciaEntrePuntos(puntoJugador, puntoEnemigo);
        DibujoDebug.dibujarString(g, String.format("%.2f",distancia), puntoX, puntoY - 8);
                
    }
    
    public void perderVida(float ataqueRecibido) {
        if (lamentoSiguiente <= 0) {
            lamento.reproducir();
            lamentoSiguiente = duracionLamento;
        }
        
        if (vidaActual - ataqueRecibido < 0) {
            vidaActual = 0;
            lamento.detenerSonido();
        } else {
            vidaActual -= ataqueRecibido;
        }
    }
    
    public void establecerPosicion(final double posicionX, final double posicionY) {
        this.posicionX = posicionX;
        this.posicionY = posicionY;
    }

    public double obtenerPosicionX() {
        return posicionX;
    }

    public double obtenerPosicionY() {
        return posicionY;
    }

    public int obtenerIdEnemigo() {
        return idEnemigo;
    }

    public float obtenerVidaActual() {
        return vidaActual;
    }
    
    public Rectangle obtenerArea() {
        final int puntoX = (int)posicionX - (int)ElementosPrincipales.jugador.obtenerPosicionX() + Constantes.MARGEN_X;
        final int puntoY = (int)posicionY - (int)ElementosPrincipales.jugador.obtenerPosicionY() + Constantes.MARGEN_Y;
        return new Rectangle(puntoX, puntoY, Constantes.LADO_SPRITE, Constantes.LADO_SPRITE);
    }
    
    public Rectangle obtenerAreaPosicional() {
        return new Rectangle((int)posicionX, (int)posicionY, Constantes.LADO_SPRITE, Constantes.LADO_SPRITE );
    }
    
    public void cambiarSiguienteNodo (Nodo nodo) {
        siguienteNodo = nodo;
    }
    
    public Nodo obtenerSiguienteNodo() {
        return siguienteNodo;
    }
    
    public static int obtenerExperiencia() {
        return exp;
    }
}

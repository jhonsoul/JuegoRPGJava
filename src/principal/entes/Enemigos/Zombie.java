
package principal.entes.Enemigos;

import java.awt.Graphics;
import principal.Constantes;
import principal.herramientas.DibujoDebug;
import principal.sprites.HojaSprites;

public class Zombie extends Enemigo{
    
    private static HojaSprites hojaZombie;
    
    
    
    
    public Zombie(final int idEnemigo, final String nombre, final int vidaMaxima, final int exp,final String rutaLamento) {
        super(idEnemigo, nombre, vidaMaxima, exp, rutaLamento);
        
        if (hojaZombie == null) {
            hojaZombie = new HojaSprites(Constantes.RUTA_ENEMIGOS + idEnemigo + ".png", Constantes.LADO_SPRITE, false);
        }
    }
    
    public void dibujar(final Graphics g, final int puntoX, final int puntoY) {
        DibujoDebug.dibujarImagenes(g, hojaZombie.obtenerSprite(0).obtenerImagen(), puntoX, puntoY);
        super.dibujar(g, puntoX, puntoY);
    }
    
    
    
}

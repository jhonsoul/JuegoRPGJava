
package principal.herramientas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class DibujoDebug {
    
    private static int OBJETOS_DIBUJADOS = 0;
    
    public static void dibujarImagenes(final Graphics g, final BufferedImage img, final int x, final int y) {
        OBJETOS_DIBUJADOS++;
        g.drawImage(img, x, y, null);
    }
    
    public static void dibujarImagenes(final Graphics g, final BufferedImage img,final Point p) {
        OBJETOS_DIBUJADOS++;
        g.drawImage(img, p.x, p.y, null);
    }
    
    public static void dibujarString(final Graphics g, final String s, final int x, final int y) {
        OBJETOS_DIBUJADOS++;
        g.drawString(s, x, y);
    }
    
    public static void dibujarString(final Graphics g, final String s, final int x, final int y, final Color c) {
        OBJETOS_DIBUJADOS++;
        g.setColor(c);
        g.drawString(s, x, y);
    }
    
    public static void dibujarString(final Graphics g, final String s, final Point p) {
        OBJETOS_DIBUJADOS++;
        g.drawString(s, p.x, p.y);
    }
    
    public static void dibujarString(final Graphics g, final String s, final Point p, final Color c) {
        OBJETOS_DIBUJADOS++;
        g.setColor(c);
        g.drawString(s, p.x, p.y);
    }
    
    public static void dibujarRectanguloRelleno(final Graphics g, final int x, final int y, final int ancho, final int alto) {
        OBJETOS_DIBUJADOS++;
        g.fillRect(x, y, ancho, alto);
    }
    
    public static void dibujarRectanguloRelleno(final Graphics g, final int x, final int y, final int ancho, final int alto, final Color c) {
        OBJETOS_DIBUJADOS++;
        g.setColor(c);
        g.fillRect(x, y, ancho, alto);
    }
    
    public static void dibujarRectanguloRelleno(final Graphics g, final Rectangle r) {
        OBJETOS_DIBUJADOS++;
        g.fillRect(r.x, r.y, r.width, r.height);
    }
    
    public static void dibujarRectanguloRelleno(final Graphics g, final Rectangle r, final Color c) {
        OBJETOS_DIBUJADOS++;
        g.setColor(c);
        g.fillRect(r.x, r.y, r.width, r.height);
    }
    
    public static void dibujarContornoRectangulo(final Graphics g, final int x, final int y, final int ancho, final int alto) {
        OBJETOS_DIBUJADOS++;
        g.drawRect(x, y, ancho, alto);
    }
    
    public static void dibujarContornoRectangulo(final Graphics g, final int x, final int y, final int ancho, final int alto, final Color c) {
        OBJETOS_DIBUJADOS++;
        g.setColor(c);
        g.drawRect(x, y, ancho, alto);
    }
    
    public static void dibujarContornoRectangulo(final Graphics g, final Rectangle r) {
        OBJETOS_DIBUJADOS++;
        g.drawRect(r.x, r.y, r.width, r.height);
    }
    
    public static void dibujarContornoRectangulo(final Graphics g, final Rectangle r, final Color c) {
        OBJETOS_DIBUJADOS++;
        g.setColor(c);
        g.drawRect(r.x, r.y, r.width, r.height);
    }
    
    public static int obtenerContadorObjetos() {
        return OBJETOS_DIBUJADOS;
    }
    
    public static void reiniciarContadorObtetos() {
        OBJETOS_DIBUJADOS = 0;
    }

}

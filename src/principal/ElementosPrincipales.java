
package principal;

import principal.entes.jugador.Jugador;
import principal.inventario.Inventario;
import principal.mapas.MapaTiled;
import principal.maquinaestados.estados.menujuego.EstructuraMenu;

public class ElementosPrincipales {
    
    public static MapaTiled mapa = new MapaTiled(Constantes.RUTA_MAPA_TILED);
    public static Jugador jugador = new Jugador();
    public static Inventario inventario = new Inventario();
    public static EstructuraMenu estructuraMenu = new EstructuraMenu();
}
